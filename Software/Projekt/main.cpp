/**************
* plik: main.cpp
* PTM laboratoria, Projekt
* prowadzacy: Mgr in�. Andrzej Stachno
* zaj�cia: Y02-68p, pon. TP 9:15
* student: Mateusz W�jcik, 258973
* II rok AiR
* Politechnika Wroc�awska
*************/

#include "./include/GLOBAL.h"
#include "./include/encoder/encoder.h"
#include "./include/lcd/LCD_HD44780.h"
#include "./include/stepper/stepper.h"
#include "./include/interface/buzzer.h"
#include "./include/interface/menu.h"
#include "./include/eeprom/eeprom.h"
#include <string.h>
#include <stdio.h>
#include <string.h>

/*Tworzone s� globalne obiekty klas u�ywanych w programie*/
encoder Encoder;
stepper Stepper;
buzzer Buzzer;
menu Menu;
LCD_HD44780 LCD;
eeprom EEPROM;

int main(){
	/*adresy zapisu poszczeg�lnych parametr�w do pami�ci eeprom */
	static uint8_t eepromSpeedAdressBegin = 0xF0;
	static uint8_t eepromAngleAdressBegin = 0xF2;
	static uint8_t eepromCyclesAdressBegin = 0xF4;
	
	/*zmienne u�ywane do odczytu parametr�w urz�dzenia z pami�ci EEPROM*/
	uint16_t eepromSpeed = 0;
	uint16_t eepromAngle = 0;
	uint16_t eepromCycles = 0;
	
	/*inicjacja obiekt�w klas u�ywanych w p�tli g�ownej programu*/
	Encoder.encInit();
	Stepper.stepperInit();
	Buzzer.buzzerInit();
	LCD.init();
	
	/*zmienne przechowuj�ce aktualne parametry pracy urz�dzenia*/
	uint16_t stepperSpeed = 0;
	uint16_t stepperAngle = 0;
	uint16_t stepperCycles = 0;
	
	/*zmienna pomocnicza przechowuj�ca ilo�� wykonanych cykli podczas pracy urz�dzenia*/
	uint16_t elapsedCycles = 0;
	
	
	/*zmienne pomocnicze, obs�uga cyklu pracy silnika krokowego, flagi pojedynczego wykonania*/
	uint8_t cycleStep = 0;
	uint8_t doonce = 0;
	uint8_t doonce1 = 0;
	
	
	LCD.goTo(0,0);
	LCD.writeText("Mateusz Wojcik");
	LCD.goTo(0,1);
	LCD.writeText("PWr      v1.1");
	
	
	_delay_ms(2000); //czekaj na wykonanie wszystkich ustawie� 
	
	sei(); //w��czenie przerwa� globalnych


	Menu.menu_Init(stepperSpeed, stepperAngle, stepperCycles);
	while(1){
	_delay_ms(1);

		switch(Menu.getMenuCounter()){
			case 0:
				switch (Menu.getSubmenuCounter()){
					case 0: //Ekran powitalny
						Menu.menu_DisplayStartScreen(LCD, Encoder);
						break;
				}
				break;
			case 1: //Sekcja "Last"
			/* Odczytaj parametry z pami�ci EEPROM */
			if(doonce1 == 0){
				eepromSpeed = EEPROM.eeprom2ByteRead(eepromSpeedAdressBegin);
				eepromAngle = EEPROM.eeprom2ByteRead(eepromAngleAdressBegin);
				eepromCycles = EEPROM.eeprom2ByteRead(eepromCyclesAdressBegin);
			}
			Menu.menu_DisplayLastScreen(LCD, Encoder, eepromSpeed, eepromAngle, eepromCycles);
			break;
			
			case 2: //Sekcja "New"
				switch(Menu.getSubmenuCounter()){
					case 2: // Sekcja ustawienia pr�dko�ci wychy�u talerza - "speed"
						Menu.menu_SpeedEditScreen(LCD, Encoder, stepperSpeed);
						break;
						
					case 1: //Sekcja ustawienia k�ta wychy�u talerza - "angle"
						Menu.menu_AngleEditScreen(LCD, Encoder, stepperAngle);
						break;
						
					case 3: //Sekcja ustawienia ilo�ci cykli wychy�u talerza - "cycles"
						Menu.menu_CyclesEditScreen(LCD, Encoder, stepperCycles);
						break;
						
					case 4: //Ekran informacyjny - zerowanie pozycji silnika z wykorzystaniem czujnika efektu Halla
						Menu.menu_HomingScreen(LCD);
						Stepper.homeStepper();
						_delay_ms(500);
						Menu.setSubmenuCounter(5);
						Menu.clearDoonce();
					break;
					
					case 5: //Zasygnalizowanie gotowo�ci, oczekiwanie na wci�ni�cie wa�u przez u�ytkownika
						Menu.menu_ConfirmWorkScreen(LCD, Encoder);
						break;
					
					case 6:
						if(doonce == 0){
					
							stepperSpeed = Menu.getSpeed();
							stepperAngle = Menu.getAngle();
							stepperCycles = Menu.getCycles();
							Menu.menu_workingScreen(LCD, Encoder, stepperCycles, 0);
							doonce = 1;
							
						} //Obs�uga cykli pracy, wy�wietlanie aktualnego post�pu
						Stepper.stepperSetSpeed(stepperSpeed);
						for( ;elapsedCycles < stepperCycles;){
							while (!Stepper.isStepperBusy()){
								switch(cycleStep){
									case 0: //podcykl 1/4, od po�o�enia zerowego w g�r�
										Stepper.planMove(stepperAngle);
										Stepper.stepperRun();
										Stepper.setStepperDir(0);
										cycleStep = 1;
										break;
			
									case 1:  //podcykl 2/4, od po�o�enia g�rnego do po�o�enia zerowego
										Stepper.planMove(360);
										Stepper.stepperRun();
										Stepper.setStepperDir(1);
										while(!(HALL_INPUT_PIN&(1<<HALL_INPUT_PORTLINE))){}
										Stepper.stepperStop(0);
										Stepper.clearPos();
										cycleStep = 2;
										break;
			
									case 2: //podcykl 3/4, od po�o�enia zerowego do po�o�enia dolnego
										Stepper.planMove(stepperAngle);
										Stepper.stepperRun();
										Stepper.setStepperDir(1);
										cycleStep = 3;
										break;
			
									case 3: //podcykl 2/4, od po�o�enia dolnego do po�o�enia zerowego
										Stepper.planMove(360);
										Stepper.stepperRun();
										Stepper.setStepperDir(0);
										while(!(HALL_INPUT_PIN&(1<<HALL_INPUT_PORTLINE))){}
										Stepper.stepperStop(0);
										Stepper.clearPos();
										cycleStep = 0;
										++elapsedCycles;
										Menu.menu_workingScreen(LCD, Encoder, stepperCycles, elapsedCycles);
										break;
			
								}
							}
						}
						Stepper.planMove(360);
						Stepper.stepperRun();
						Stepper.setStepperDir(0);
						while(!(HALL_INPUT_PIN&(1<<HALL_INPUT_PORTLINE))){}
						Stepper.stepperStop(0);
						Stepper.clearPos();
						Buzzer.buzzer3Beeps();
						Menu.clearDoonce();
						Menu.clearSwitch();
						Menu.setSubmenuCounter(7);
						break;
					
					case 7: //wy�wietlenie ekranu powiadaj�cego o zako�czeniu pracy
						Menu.menu_FinishedWorkScreen(LCD, Encoder);
						
						//zapis parametr�w pracy do pami�ci eeprom je�li r�ni� si� one od obecnie zapisanych w pami�ci
						if(stepperSpeed!=EEPROM.eeprom2ByteRead(eepromSpeedAdressBegin))
							EEPROM.eeprom2ByteSave(eepromSpeedAdressBegin,stepperSpeed);
							
						if(stepperSpeed!=EEPROM.eeprom2ByteRead(eepromAngleAdressBegin))
							EEPROM.eeprom2ByteSave(eepromAngleAdressBegin,stepperAngle);
						
						if(stepperSpeed!=EEPROM.eeprom2ByteRead(eepromCyclesAdressBegin))
							EEPROM.eeprom2ByteSave(eepromCyclesAdressBegin,stepperCycles);
						
						elapsedCycles = 0;
						doonce = 0;
						doonce1 = 0;
						Stepper.stepperStop(1);	
						break;
						
			}
		}
	}			
}


	
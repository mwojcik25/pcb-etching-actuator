/**************
* plik: eeprom.h
* PTM laboratoria, Projekt
* prowadzacy: Mgr in�. Andrzej Stachno
* zaj�cia: Y02-68p, pon. TP 9:15
* student: Mateusz W�jcik, 258973
* II rok AiR
* Politechnika Wroc�awska
*************/
#ifndef EEPROM_H_
#define EEPROM_H_
#include "../GLOBAL.h"

/*************
* Klasa eeprom jest klas� pozwalaj�c� na zapis oraz odczyt parametr�w urz�dzenia w pami�ci nieulotnej EEPROM mikrokontrolera
* Dla klasy zosta�y metody pozwalaj�ce na zapis/odczyt warto�ci zar�wno 8-, jak i 16-bitowych.
*************/
class eeprom{
	/* metody publiczne klasy eeprom */
	public:
	
	/* Metoda eeprom1ByteSave pozwala na zapis warto�ci 8-bitowej pod przekazany adres.
	*parametry metody:
	*address - 16-bitowy adres kom�rki pami�ci, do kt�rej dana warto�� ma zosta� zapisana
	*data - 8bitowa warto�� zapisywana do wskazanej kom�rki pami�ci
	*/
	void eeprom1ByteSave(uint16_t address, uint8_t data);
	
	
	/* Metoda eeprom2ByteSave pozwala na zapis warto�ci 16-bitowej pod przekazany adres. Ze wzgl�du na to, �e struktura pami�ci mikrokontrolera przewiduje s�owo 8-bitowe, by zapisa� warto�ci wi�ksze
	*musimy skorzysta� z wi�kszej ilo�ci kom�rek pami�ci -> warto�ci 16-bitowe zapisywane s� w�wczas w dw�ch kom�rkach pami�ci: podana jako pocz�tek oraz kom�rka nast�pna.
	*parametry metody:
	*beginAddress - 16-bitowy adres pierwszej z kom�rek pami�ci, do kt�rych dana warto�� ma zosta� zapisana. 
	*data - 16-bitowa warto�� zapisywana do wskazanej kom�rki pami�ci
	*/
	void eeprom2ByteSave(uint16_t beginAddress, uint16_t data);
	
	
	/* Metoda eeprom1ByteRead pozwala na odczyt warto�ci 8-bitowej z kom�rki pami�ci, kt�rej adres zosta� przekazany jako parametr.
	*parametry metody:
	*address - 16-bitowy adres kom�rki pami�ci, z kt�rej ma zosta� dokonany odczyt danych. 
	*return - metoda zwraca odczytan� warto�� 8-bitow� z kom�rki pami�ci o podanym adresie. 
	*/
	uint8_t eeprom1ByteRead(uint16_t address);	
	
	
	/* Metoda eeprom2ByteRead pozwala na odczyt warto�ci 16-bitowej z kom�rek pami�ci o przekazanym adresie. Ze wzgl�du na to, �e struktura pami�ci mikrokontrolera przewiduje s�owo 8-bitowe, by odczyta� warto�ci wi�ksze
	*musimy skorzysta� z wi�kszej ilo�ci kom�rek pami�ci -> warto�ci 16-bitowe s� w�wczas w odczytywane z dw�ch kom�rek pami�ci: podanej jako pocz�tek oraz kom�rki nast�pnej.
	*parametry metody:
	*beginAddress - 16-bitowy adres pierwszej z kom�rek pami�ci, z kt�rych warto�� chcemy odczyta�.
	*return - metoda zwraca 16-bitow� warto�� odczytan� z kom�rek pami�ci o przekazanym adresie pocz�tkowym.
	*/
	uint16_t eeprom2ByteRead(uint16_t beginAddress);
};





#endif /* EEPROM_H_ */
/**************
* plik: eeprom.cpp
* PTM laboratoria, Projekt 
* prowadzacy: Mgr in�. Andrzej Stachno
* zaj�cia: Y02-68p, pon. TP 9:15
* student: Mateusz W�jcik, 258973
* II rok AiR
*************/
#include "eeprom.h"



void eeprom::eeprom1ByteSave(uint16_t address, uint8_t data){
	cli(); //wy��cz przerwania globalne w celu unikni�cia b��d�w w pami�ci EEPROM
	
	while(EECR & (1<<EEPE)){} //zaczekaj na dost�pno�� zapisu do EEPROM
	
	/* ustaw adres zapisu oraz dane do zapisania */
	EEAR = address;
	EEDR = data;
						
	EECR&=!(1<<EEPE); //wyzeruj flag� zapisu przed w��czeniem flagi zapisu master
	EECR |= (1<<EEMPE); //w��cz flag� zapisu master
	EECR |= (1<<EEPE); //zainicjuj zapis do EEPROM
	
	while((EECR & (1<<EEPE))){} //czekaj dop�ki zapis nie zostanie zako�czony
		
	sei(); 	//w��cz z powrotem przerwania globalne
}




void eeprom::eeprom2ByteSave(uint16_t beginAddress, uint16_t data){
	/* przygotuj zmienne pomocnicze na zapis warto�ci 16-bitowej */
	uint8_t dataL = 0; 
	uint8_t dataH = 0;	
	
	dataL = data&0xFF; //ustaw 8 m�odszych bit�w zapisywanej zmiennej
	dataH = data&0xFF00; //ustaw 8 starszych bit�w zapisywanej zmiennej
	
	eeprom1ByteSave(beginAddress, dataH); //zapisz 8 starszych bit�w pod pocz�tkowy adres
	eeprom1ByteSave(beginAddress+1, dataL); //zapisz 8 m�odszych bit�w pod adres nast�pny
}
	
uint8_t eeprom::eeprom1ByteRead(uint16_t address){
	uint8_t readData = 0;
	
	cli(); //wy��cz przerwania globalne w celu unikni�cia b��d�w w pami�ci EEPROM
	
	while((EECR & (1<<EEPE))){} //zaczekaj na dost�pno�� EEPROM
	
	EEAR = address; //ustaw adres odczytu
	
	EECR|=(1<<EERE); //zainicjuj odczyt EEPROM
	
	readData = EEDR; //przepisz odczytane dane do zmiennej
	
	while((EECR & (1<<EEPE))){} //upewnij si� czy odczyt zosta� zako�czony
	
	sei(); //w��cz z powrotem przerwania globalne
	
	return readData; //zwr�� odczytane dane
	
}
uint16_t eeprom::eeprom2ByteRead(uint16_t beginAddress){
	/* przygotuj zmienne pomocnicze na odczyt warto�ci 16-bitowej */
	uint8_t dataL = 0; 
	uint8_t dataH = 0;
	
	uint16_t data = 0; //zainicjuj zmienn� do zwr�cenia odczytanej warto�ci
	
	dataH = eeprom1ByteRead(beginAddress); //odczytaj starsze 8 bit�w z adresu pocz�tkowego
	dataL = eeprom1ByteRead(beginAddress +1); //odczytaj m�odsze 8 bit�w z adresu nast�pnego
	
	data = (dataH<<8); //wpisz starsze 8 bit�w do zainicjowanej zmiennej 16-bitowej, przesu� o 8 pozycji w lewo
	data = data | dataL; //dodaj m�odsze 8 bit�w do przesuni�tych bit�w starszych
	
	return data; //zwr�� po��czone dane
}
/**************
* plik: buzzer.h
* PTM laboratoria, Projekt
* prowadzacy: Mgr in�. Andrzej Stachno
* zaj�cia: Y02-68p, pon. TP 9:15
* student: Mateusz W�jcik, 258973
* II rok AiR
* Politechnika Wroc�awska
*************/

#ifndef BUZZER_H_
#define BUZZER_H_

#include "../GLOBAL.h"

/** Konfiguracja portu pod��czenia buzzera **/
#define BUZZER_DDR DDRD
#define BUZZER_PORT PORTD
#define BUZZER_PORTLINE PD0

/*************
* Klasa buzzer jest klas� pozwalaj�c� na komunikacj� z u�ytkownikiem za pomoc� sygna��w d�wi�kowych, z wykorzystaniem brz�czyka. 
*************/
class buzzer{
	
	/*** metody publiczne klasy eeprom **/
	public:
	
	/*Metoda buzzerInit jest metod� inicjalizuj�c� lini� portu do sterowania brz�czykiem */
	void buzzerInit(void);
	
	/*Metoda buzzerLongBeep pozwala na emisj� d�ugiego sygna�u d�wi�kowego */
	void buzzerLongBeep(void);
	
	/*Metoda buzzer3Beeps pozwala na emisj� sygna�u d�wi�kowego sk�adaj�cego si� z trzech kr�tkich  */
	void buzzer3Beeps(void);	
};



#endif /* BUZZER_H_ */
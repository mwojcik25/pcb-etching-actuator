/**************
* plik: menu.cpp
* PTM laboratoria, Projekt
* prowadzacy: Mgr in�. Andrzej Stachno
* zaj�cia: Y02-68p, pon. TP 9:15
* student: Mateusz W�jcik, 258973
* II rok AiR
* Politechnika Wroc�awska
*************/
#include "menu.h"


void menu::menu_Init(uint16_t &Speed, uint16_t &Angle, uint16_t &Cycles){
	_storedSpeed = DEFAULTSPEED;
	_storedAngle = DEFAULTANGLE;
	_storedCycles = DEFAULTCYCLES;
	
	Speed = _storedSpeed;
	Angle = _storedAngle;
	Cycles = _storedCycles;
}

void menu::menu_DisplayStartScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance){
	if(!doonce1){
		_pswState = 1;
		encoderInstance.setPosition(_value);
		doonce1 = 1;
		
		lcdInstance.clear();
	
		lcdInstance.goTo(1,0);
		lcdInstance.writeText("PCB Etch Mixer");
	
		lcdInstance.goTo(0,1);
		lcdInstance.writeText("Last");
	
		lcdInstance.goTo(13,1);
		lcdInstance.writeText("New");
	}
	if(encoderInstance.getSwState()!=_pswState){
		if(encoderInstance.getSwState()){
			_switch = 1;
		}
		_pswState = encoderInstance.getSwState();
	}else
		_switch = 0;
		
	_value = encoderInstance.getPosition();
	
	if(_value > 2){
		encoderInstance.setPosition(2);
		_value = encoderInstance.getPosition();
	}
	
	if(_value < 1){
		encoderInstance.setPosition(1);
		_value = encoderInstance.getPosition();
	}
	switch(_value){ //przej�cie do sekcji "LAST"
		case 1:
			lcdInstance.goTo(4,1);
			lcdInstance.writeData('<');
			lcdInstance.goTo(12,1);
			lcdInstance.writeData(' ');
			if(_switch){
				_menuCounter = 1;
				_submenuCounter	= 1;
				_value = 0;
				clearDoonce();
			}
			break;
			
		case 2: //przej�cie do ekran�w ustawie� parametr�w urz�dzenia i wykonaniacyklu pracy
			lcdInstance.goTo(4,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(12,1);
			lcdInstance.writeData('>');
			if(_switch){
				_menuCounter = 2;
				_submenuCounter	= 1;
				_value = 0;
				clearDoonce();
			}
			break;
	}

		
}

void menu::menu_SpeedEditScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance, uint16_t &Speedvalue){
	double tiltPerMinute = (double) _storedSpeed /_storedAngle;
	tiltPerMinute =  4 *  tiltPerMinute * 15;
	if(!doonce1){
		_pvalue = 0;
		encoderInstance.setPosition( _value);
		lcdInstance.clear();
		lcdInstance.goTo(0,0);
		lcdInstance.writeText("Speed [t/min]: ");
		lcdInstance.printNumber((uint16_t) tiltPerMinute, 7, 1);
		doonce1 = 1;
	}

	if(encoderInstance.getSwState()!=_pswState){
		if(encoderInstance.getSwState()){
			_switch = 1;
		}
		_pswState = encoderInstance.getSwState();
	}else
	_switch = 0;


	if(_valueSelection == 1){
		_value = (encoderInstance.getPosition());
		if(_value > MAXSPEED){
			encoderInstance.setPosition(720);
			_value = encoderInstance.getPosition();
			}if(_value < MINSPEED){
			encoderInstance.setPosition(15);
			_value = encoderInstance.getPosition();
		}
		if(_value!=_pvalue){
			tiltPerMinute = (double) _value / (double)_storedAngle;
			tiltPerMinute = 4*  tiltPerMinute * 15 ;
			lcdInstance.printNumber((uint16_t) tiltPerMinute, 7, 1);
			lcdInstance.goTo(0,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(10,1);
			lcdInstance.writeData('<');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData(' ');
		}
		_pvalue = _value;
		if(_switch && _valueSelection){
			lcdInstance.goTo(0,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(10,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData(' ');
			_valueSelection = 0;
			_storedSpeed = _value;
			Speedvalue = _value;
			encoderInstance.setPosition(2);
			_switch = 0;
		}
	}
	
	
	if(_valueSelection == 0){
		_value = encoderInstance.getPosition();
		if(_value > 3){
			encoderInstance.setPosition(3);
			_value = encoderInstance.getPosition();
			}if(_value < 1){
			encoderInstance.setPosition(1);
			_value = encoderInstance.getPosition();
		}

		switch(_value){
			case 1:
			lcdInstance.goTo(0,1);
			lcdInstance.writeData('<');
			lcdInstance.goTo(5,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData(' ');
			if(_switch){
				_menuCounter = 2;
				_submenuCounter	= 1;
				_value = 0;
				clearDoonce();
			}
			break;
			
			case 2:
			lcdInstance.goTo(0,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(5,1);
			lcdInstance.writeData('>');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData(' ');
			if(_switch){
				_valueSelection = 1;
				encoderInstance.setPosition(_storedSpeed);
				lcdInstance.goTo(0,1);
				lcdInstance.writeData(' ');
				lcdInstance.goTo(10,1);
				lcdInstance.writeData('<');
				lcdInstance.goTo(15,1);
				lcdInstance.writeData(' ');
			}
			break;
			
			case 3:
			lcdInstance.goTo(0,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(5,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData('>');
			if(_switch){
				_menuCounter = 2;
				_submenuCounter	= 3;
				_value = 0;
				clearDoonce();
			}
			break;
		}

	}
}

void menu::menu_AngleEditScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance, uint16_t &Anglevalue){
	if(!doonce1){
		_pvalue = 0;
		encoderInstance.setPosition(_value);
		lcdInstance.clear();
		lcdInstance.goTo(0,0);
		lcdInstance.writeText("Angle [%]: ");
		lcdInstance.printNumber(((_storedAngle * 100)/MAXANGLE), 7, 1);
		doonce1 = 1;
	}
	
	
	

if(encoderInstance.getSwState()!=_pswState){
	if(encoderInstance.getSwState()){
		_switch = 1;
	}
	_pswState = encoderInstance.getSwState();
}else
_switch = 0;
	
	
	if(_valueSelection == 1){
		_value = encoderInstance.getPosition();
		if(_value > MAXANGLE){
			encoderInstance.setPosition(MAXANGLE);
			_value = encoderInstance.getPosition();
			}if(_value < MINANGLE){
			encoderInstance.setPosition(MINANGLE);
			_value = encoderInstance.getPosition();
		}
		if(_value!=_pvalue){
			lcdInstance.printNumber(((_value * 100)/MAXANGLE), 7, 1);
			lcdInstance.goTo(0,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(10,1);
			lcdInstance.writeData('<');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData(' ');
		}
		_pvalue = _value;
		if(_switch && _valueSelection){
			lcdInstance.goTo(0,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(10,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData(' ');
			_valueSelection = 0;
			_storedAngle = _value;
			Anglevalue = _value;
			encoderInstance.setPosition(2);
			_switch = 0;

		}
	}
	
	
	if(_valueSelection == 0){
		_value = encoderInstance.getPosition();
		if(_value > 3){
			encoderInstance.setPosition(3);
			_value = encoderInstance.getPosition();
			}if(_value < 1){
			encoderInstance.setPosition(1);
			_value = encoderInstance.getPosition();
		}
		switch(_value){
			case 1:
			lcdInstance.goTo(0,1);
			lcdInstance.writeData('<');
			lcdInstance.goTo(5,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData(' ');
			if(_switch){
				_menuCounter = 0;
				_submenuCounter	= 0;
				_value = 0;
				clearDoonce();

			}
			break;
			
			case 2:
			lcdInstance.goTo(0,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(5,1);
			lcdInstance.writeData('>');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData(' ');
			if(_switch){
				_valueSelection = 1;
				encoderInstance.setPosition(_storedAngle);
				lcdInstance.goTo(0,1);
				lcdInstance.writeData(' ');
				lcdInstance.goTo(10,1);
				lcdInstance.writeData('<');
				lcdInstance.goTo(15,1);
				lcdInstance.writeData(' ');
			}
			
			break;
			
			case 3:
			lcdInstance.goTo(0,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(5,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData('>');
			if(_switch){
				_menuCounter = 2;
				_submenuCounter	= 2;
				_value = 0;
				clearDoonce();

			}
			break;
		}

	}
}

void menu::menu_CyclesEditScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance, uint16_t &Cyclesvalue){
	double minutes = 0;
	double seconds = 0;
	uint8_t hours = 0;
	if(!doonce1){
		encoderInstance.setPosition(_value);
		_pvalue = 0;		
		lcdInstance.clear();
		lcdInstance.goTo(0,0);
		lcdInstance.writeText("Time: ");
		seconds = (4 * (double)_storedCycles * (double)_storedAngle)/((double)_storedSpeed);
		minutes = minutes + (seconds/60);
		seconds = ((uint16_t)seconds) %60;
		hours = (uint8_t)(minutes/60);
		minutes = (uint16_t) minutes%60;
		lcdInstance.printNumber(hours, 8, 0);
		lcdInstance.printNumber(((uint16_t) minutes), 10, 0);
		lcdInstance.printNumber(((uint16_t) seconds), 13, 0);
		lcdInstance.goTo(11,1);
		lcdInstance.writeText("[C]");
		lcdInstance.goTo(15,0);
		lcdInstance.writeData('s');
		lcdInstance.goTo(12,0);
		lcdInstance.writeData('m');
		lcdInstance.goTo(9,0);
		lcdInstance.writeData('h');
		lcdInstance.printNumber(_storedCycles, 6, 1);
		doonce1 = 1;
		
	}
	
	
	

if(encoderInstance.getSwState()!=_pswState){
	if(encoderInstance.getSwState()){
		_switch = 1;
	}
	_pswState = encoderInstance.getSwState();
}else
_switch = 0;
	
	
	if(_valueSelection == 1){
		_value = encoderInstance.getPosition();
		if(_value > MAXCYCLES){
			encoderInstance.setPosition(MAXCYCLES);
			_value = encoderInstance.getPosition();
			}if(_value < MINCYCLES){
			encoderInstance.setPosition(MINCYCLES);
			_value = encoderInstance.getPosition();			
		}
		if(_value!=_pvalue){
			seconds = (4 * (double)_value * (double)_storedAngle)/((double)_storedSpeed);
			minutes = minutes + (seconds/60);
			seconds = ((uint16_t)seconds) %60;
			hours = (uint8_t)(minutes/60);
			minutes = (uint16_t) minutes%60;
			lcdInstance.printNumber(hours, 8, 0);
			lcdInstance.printNumber(((uint16_t) minutes), 10, 0);
			lcdInstance.printNumber(((uint16_t) seconds), 13, 0);			
			lcdInstance.goTo(15,0);
			lcdInstance.writeData('s');
			lcdInstance.goTo(12,0);
			lcdInstance.writeData('m');
			lcdInstance.goTo(9,0);
			lcdInstance.writeData('h');
			lcdInstance.printNumber(_value, 6, 1);
		}
		_pvalue = _value;
		lcdInstance.goTo(0,1);
		lcdInstance.writeData(' ');
		lcdInstance.goTo(9,1);
		lcdInstance.writeData('<');
		lcdInstance.goTo(15,1);
		lcdInstance.writeData(' ');
		if(_switch && _valueSelection){

			lcdInstance.goTo(9,1);
			lcdInstance.writeData(' ');
			_valueSelection = 0;
			_storedCycles = _value;
			Cyclesvalue = _value;
			encoderInstance.setPosition(2);
			_switch = 0;
		}
	}
		
	
	if(_valueSelection == 0){
	_value = encoderInstance.getPosition();
		if(_value > 3){
			encoderInstance.setPosition(3);
			_value = encoderInstance.getPosition();
		}if(_value < 1){
			encoderInstance.setPosition(1);
			_value = encoderInstance.getPosition();
}
		switch(_value){
			case 1:
			lcdInstance.goTo(0,1);
			lcdInstance.writeData('<');
			lcdInstance.goTo(5,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData(' ');
			if(_switch){
				_menuCounter = 2;
				_submenuCounter	= 2;
				_value = 0;
				clearDoonce();
			}
			break;
			
			case 2:
			lcdInstance.goTo(0,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(5,1);
			lcdInstance.writeData('>');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData(' ');
			if(_switch){
				_valueSelection = 1;
				encoderInstance.setPosition(_storedCycles);
				lcdInstance.goTo(0,1);
				lcdInstance.writeData(' ');
				lcdInstance.goTo(9,1);
				lcdInstance.writeData('<');
				lcdInstance.goTo(15,1);
				lcdInstance.writeData(' ');
			}
			
			break;
			
			case 3:
			lcdInstance.goTo(0,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(5,1);
			lcdInstance.writeData(' ');
			lcdInstance.goTo(15,1);
			lcdInstance.writeData('>');
			if(_switch){
				_menuCounter = 2;
				_submenuCounter	= 4;
				_value = 0;
				clearDoonce();
			}
			break;
		}

	}
}

void menu::menu_HomingScreen(LCD_HD44780 &lcdInstance){
	if(doonce1==0){
		_pswState = 1;
		lcdInstance.clear();
		lcdInstance.goTo(0,0);
		lcdInstance.writeText("STEPPER");
		lcdInstance.goTo(3,1);
		lcdInstance.writeText("HOMING");
		doonce1 = 1;
	}
	_value = 0;
	clearSwitch();
}
	
void menu::menu_ConfirmWorkScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance){
		if(!doonce1){
			_pswState = 1;
			doonce1 = 1;
			
			lcdInstance.clear();
			
			lcdInstance.goTo(0,0);
			lcdInstance.writeText("Press button");
			lcdInstance.goTo(5,1);
			lcdInstance.writeText("to begin...");
		}
		while(!_switch){

if(encoderInstance.getSwState()!=_pswState){
	if(encoderInstance.getSwState()){
		_switch = 1;
	}
	_pswState = encoderInstance.getSwState();
}else
_switch = 0;
		}
			clearDoonce();
			_value = 0;
			_submenuCounter = 6;
	
}

void menu::menu_workingScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance, uint16_t nCycles, uint16_t actualCycles){
		
		if(encoderInstance.getSwState()!=_pswState){
			if(encoderInstance.getSwState()){
				_switch = 1;
			}
			_pswState = encoderInstance.getSwState();
		}else
		_switch = 0;
		
		if(!doonce1){
			_pswState = 1;
			lcdInstance.clear();
			
			lcdInstance.goTo(0,0);
			lcdInstance.writeText("Working...");
			doonce1 = 1;
		}
			

			lcdInstance.printNumber(actualCycles, 0,1);
			lcdInstance.printNumber(nCycles,13,1);
			lcdInstance.goTo(3,1);
			lcdInstance.progressBar(actualCycles, nCycles, 10);

}

void menu::menu_FinishedWorkScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance){
	if(!doonce1){
		_pswState = 1;
		doonce1 = 1;
		
		lcdInstance.clear();
		
		lcdInstance.goTo(0,0);
		lcdInstance.writeText("Finished! Press");
		lcdInstance.goTo(2,1);
		lcdInstance.writeText("to release...");
	}
	while(!_switch){

		if(encoderInstance.getSwState()!=_pswState){
			if(encoderInstance.getSwState()){
				_switch = 1;
			}
			_pswState = encoderInstance.getSwState();
		}else
		_switch = 0;
	}
	clearDoonce();
	_value = 0;
	_submenuCounter = 0;
	_menuCounter = 0;
	}


void menu::menu_DisplayLastScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance, uint16_t eepromSpeed, uint16_t eepromAngle, uint16_t eepromCycles){

	if(!doonce1){
		_pswState = 1;
		encoderInstance.setPosition(_value);
		
		lcdInstance.clear();
		
		lcdInstance.goTo(0,0);
		lcdInstance.writeText("Last:");

		lcdInstance.goTo(6,0);
		lcdInstance.writeText("S");
		double tiltPerMinute = (double) _storedSpeed /_storedAngle;
		tiltPerMinute =  4 *  tiltPerMinute * 15;
		lcdInstance.printNumber(tiltPerMinute, 7,0);
		

		uint16_t anglePercent = (eepromAngle*100)/MAXANGLE;

		lcdInstance.goTo(11,0);
		lcdInstance.writeText("A");
		lcdInstance.printNumber(anglePercent, 12,0);
		lcdInstance.goTo(15,0);
		lcdInstance.writeText("%");
		
		lcdInstance.goTo(11,1);
		lcdInstance.writeText("C");
		lcdInstance.printNumber(eepromCycles, 12,1);
		
		double minutes = (4 * (double) eepromCycles * (double)eepromAngle)/((double)eepromSpeed * 60.0);
		double seconds = (4 * (double) eepromCycles * (double)eepromAngle)/((double)eepromSpeed);
		minutes = minutes + (seconds/60);
		seconds = ((uint16_t)seconds) %60;
		uint8_t hours = (uint8_t)(minutes/60);
		minutes = (uint16_t) minutes%60;
		lcdInstance.printNumber(hours, 2, 1);
		lcdInstance.printNumber(((uint16_t) minutes), 4, 1);
		lcdInstance.printNumber(((uint16_t) seconds), 7, 1);

		lcdInstance.goTo(1,1);
		lcdInstance.writeText("T");
		lcdInstance.goTo(9,1);
		lcdInstance.writeData('s');
		lcdInstance.goTo(6,1);
		lcdInstance.writeData('m');
		lcdInstance.goTo(3,1);
		lcdInstance.writeData('h');
		
	doonce1 = 1;	
	}
	

	if(encoderInstance.getSwState()!=_pswState){
		if(encoderInstance.getSwState()){
			_switch = 1;
		}
		_pswState = encoderInstance.getSwState();
	}else
	_switch = 0;	
	
	_value = encoderInstance.getPosition();
	
	if(_value > 2){
		encoderInstance.setPosition(2);
		_value = encoderInstance.getPosition();
	}
	
	if(_value < 1){
		encoderInstance.setPosition(1);
		_value = encoderInstance.getPosition();
	}
	switch(_value){
		case 1:
		lcdInstance.goTo(0,1);
		lcdInstance.writeData('<');
		lcdInstance.goTo(15,1);
		lcdInstance.writeData(' ');
		if(_switch){
			_menuCounter = 0;
			_submenuCounter	= 0;
			_value = 0;
			clearDoonce();
		}
		break;
		
		case 2:
		lcdInstance.goTo(0,1);
		lcdInstance.writeData(' ');
		lcdInstance.goTo(15,1);
		lcdInstance.writeData('>');
		if(_switch){
			_storedSpeed = eepromSpeed;
			_storedAngle = eepromAngle;
			_storedCycles = eepromCycles;
			_menuCounter = 2;
			_submenuCounter	= 4;
			_value = 0;
			clearDoonce();
		}
		break;
	}
}

void menu::clearSwitch(void){
		_switch = 0;
		_pswState = 0;
	}
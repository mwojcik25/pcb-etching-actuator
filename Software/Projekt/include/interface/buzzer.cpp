/**************
* plik: buzzer.cpp
* PTM laboratoria, Projekt
* prowadzacy: Mgr in�. Andrzej Stachno
* zaj�cia: Y02-68p, pon. TP 9:15
* student: Mateusz W�jcik, 258973
* II rok AiR
* Politechnika Wroc�awska
*************/
#include "buzzer.h"

void buzzer::buzzerInit(void){
	BUZZER_DDR|=(1<<BUZZER_PORTLINE);	//zainicjuj lini� brz�czyka jako wyj�cie
}

void buzzer::buzzerLongBeep(void){
	BUZZER_PORT|=(1<<BUZZER_PORTLINE); //w��cz brz�czyk, odczekaj sekund�, wy��cz brz�czyk
	_delay_ms(1000);
	BUZZER_PORT&=!(1<<BUZZER_PORTLINE);
}


void buzzer::buzzer3Beeps(void){	
	BUZZER_PORT|=(1<<BUZZER_PORTLINE); //wykonaj na linii brz�czyka 3 impulsy o czasie trwania 50ms
	_delay_ms(50);
	BUZZER_PORT&=!(1<<BUZZER_PORTLINE);
	_delay_ms(50);

	BUZZER_PORT|=(1<<BUZZER_PORTLINE);
	_delay_ms(50);
	BUZZER_PORT&=!(1<<BUZZER_PORTLINE);
	_delay_ms(50);
	
	BUZZER_PORT|=(1<<BUZZER_PORTLINE);
	_delay_ms(50);
	BUZZER_PORT&=!(1<<BUZZER_PORTLINE);
	_delay_ms(50);
}
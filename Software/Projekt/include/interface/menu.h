/**************
* plik: menu.h
* PTM laboratoria, Projekt
* prowadzacy: Mgr in�. Andrzej Stachno
* zaj�cia: Y02-68p, pon. TP 9:15
* student: Mateusz W�jcik, 258973
* II rok AiR
* Politechnika Wroc�awska
*************/


#ifndef MENU_H_
#define MENU_H_

#include "../GLOBAL.h"
#include "../lcd/LCD_HD44780.h"
#include "../encoder/encoder.h"
#include <string.h>


/* Konfiguracja maksymalnych, minimalnych oraz domy�lnych warto�ci*/
/* Pr�dko�� ruch�w w stopniach na sekund�*/
#define MINSPEED 15
#define DEFAULTSPEED 60
#define MAXSPEED 360

/* K�t obrotu wa�u w stopniach */
#define MINANGLE 5
#define DEFAULTANGLE 45
#define MAXANGLE 90

/* Liczba cykli */
#define MINCYCLES 10
#define DEFAULTCYCLES 100
#define MAXCYCLES 999





/*************
* Klasa encoder jest klas� odpowiedzialn� za wy�wietlanie na ekranie zastosowanego wy�wietlacza LCD kolejnych ekran�w konfiguracyjnych.
* Metody klasy korzystaj� z istniej�cych ju� instancji obiekt�w klas wy�wietlacza LCD oraz enkodera. Za pomoc� metod w klasie menu ustawiane s� parametry
* pracy urz�dzenia, takie jak pr�dko�� poruszania talerzem, k�t wychylenia oraz liczba cykli.
*************/
class menu{

	/*metody publiczne klasy menu*/
	public:
	
	/*Metoda menu_Init odpowiada za inicjalizacj� wy�wietlacza, enkodera oraz nadpisanie parametr�w urz�dzenia parametrami domy�lnymi*/
	void menu_Init(uint16_t &Speed, uint16_t &Angle, uint16_t &Cycles);
	
	/*Metoda menu_DisplayStartScreen odpowiedzialna jest za wyswietlanie ekranu powitalnego urz�dzenia. Interakcja z ekranem odbywa si� za pomoc� przekazanych 
	 * jako referencje instancji wy�wietlacza LCD oraz enkodera*/ 
	void menu_DisplayStartScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance);
	
	/*Metoda menu_SpeedEditScreen s�u�y do ustawienia parametru pr�dko�ci przechylania talerza urz�dzenia*/ 
	void menu_SpeedEditScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance, uint16_t &Speedvalue);
	
	/*Metoda menu_AngleEditScreen s�u�y do ustawienia parametru k�tu przechy�u talerza urz�dzenia*/ 
	void menu_AngleEditScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance, uint16_t &Anglevalue);
	
	/*Metoda menu_CyclesEditScreen s�u�y do ustawienia ilo�ci cykli przechy�u talerza urz�dzenia*/ 
	void menu_CyclesEditScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance, uint16_t &Cyclesvalue);
	
	/*z pomoc� metody menu_HomingScreen wywo�ywany jest ekran informuj�cy u�ytkownika o zerowaniu pozycji wa�u silnika krokowego*/
	void menu_HomingScreen(LCD_HD44780 &lcdInstance);
	
	/*z pomoc� metody menu_ConfirmWorkScreen wywo�ywany jest ekran informuj�cy u�ytkownika o gotowo�ci do pracy. U�ytkownik mo�e wcisn�� wa� enkodera w celu rozpocz�cia pracy*/
	void menu_ConfirmWorkScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance);
	
	/*metod� menu_workingScreen wywo�ywany jest ekran pracy urz�dzenia. Ekran informuje o post�pie pracy oraz ilo�ci zaplanowanych cykli*/
	void menu_workingScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance, uint16_t nCycles, uint16_t actualCycles);
	
	/*Metoda menu_FinishedWorkScreen powiadamia u�ytkownika o zako�czeniu pracy urz�dzenia. Uzytkownik mo�e wcisn�� wa� enkodera by powr�ci� do ekranu powitalnego*/
	void menu_FinishedWorkScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance);
	
	/*Metoda menu_DisplayLastScreen odpowiada za wy�wietlenie ostatnich ustawie� zapisanych w pami�ci EEPROM urz�dzenia. U�ytkownik mo�e uruchomi� prac� z ostatnimi parametrami za pomoc�
	 * wybrania w�a�ciwej opcji na ekranie*/
	void menu_DisplayLastScreen(LCD_HD44780 &lcdInstance, encoder &encoderInstance, uint16_t eepromSpeed, uint16_t eepromAngle, uint16_t eepromCycles);
		
	/*Metody pomocnicze pozwalaj�ce na wyczyszczenie flag pomocniczych klasy menu*/	
	void clearDoonce(void){doonce1 = 0;}
	void clearSwitch(void);
	
	
	/*Metody pomocnicze s�u��ce do zwracania lub ustawiania licznik�w ekran�w oraz podekran�w w klasie menu*/
	uint8_t getMenuCounter(void){return _menuCounter;}
	uint8_t getSubmenuCounter(void){return _submenuCounter;}
	uint8_t setSubmenuCounter(uint8_t newCounter){ _submenuCounter = newCounter;}		
		
		
	/*Metody pozwalaj�ce na zwr�cenie ustawionych przez menu parametr�w roboczych urz�dzenia*/	
	uint16_t getSpeed(void){return _storedSpeed;}
	uint16_t getAngle(void){return _storedAngle;}
	uint16_t getCycles(void){return _storedCycles;}
		
		
	/* Metody pozwalaj�ce na zwr�cenie przyci�ni�cia wa�u enkodera oraz ustawianej w menu warto�ci pokr�t�a enkodera*/	
	uint8_t getSw(void){return _switch;}	
	int16_t getValue(void){return _value;}
	
	
	private:
	/*Pola prywatne klasy menu*/
	/*liczniki ekran�w oraz podekran�w menu*/
	uint8_t _menuCounter = 0;   
	uint8_t _submenuCounter = 0;
	
	uint8_t _valueSelection = 0; //flaga wyboru warto�ci ustawiana podczas wyboru parametr�w pracy urz�dzenia
	
	int16_t _value = 0;  //zmienna odpowiedzialna za przechowywanie warto�ci wybranej przez enkoder
	uint8_t doonce1 = 0; //flaga pomocnicza pojedynczego wykonania
	uint8_t _switch = 0; //flaga odpowiedzialna za przechowywanie stan przycisniecia wa�u enkodera
	uint8_t _pswState = 0; //flaga odpowiedzialna za przechowywanie poprzedniego stanu przycisniecia wa�u enkodera
	int16_t _pvalue = 0;  //zmienna odpowiedzialna za przechowywanie poprzedniej warto�ci wybranej przez ekoder
	
	/*zmienne przechowuj�ce ustawione warto�ci parametr�w urz�dzenia*/
	uint16_t _storedSpeed = 60;
	uint16_t _storedAngle = 60;
	uint16_t _storedCycles =30;
	
};




#endif /* MENU_H_ */
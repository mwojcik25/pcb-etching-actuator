/**************
* plik: encoder.h
* PTM laboratoria, Projekt
* prowadzacy: Mgr in�. Andrzej Stachno
* zaj�cia: Y02-68p, pon. TP 9:15
* student: Mateusz W�jcik, 258973
* II rok AiR
* Politechnika Wroc�awska
*************/

#ifndef ENCODER_H_
#define ENCODER_H_

#include "../GLOBAL.h"

/* Konfiguracja pod��czenia enkodera do mikrokontrolera */
#define ENCODER_CLK_DDR DDRD
#define ENCODER_CLK_PIN PIND
#define ENCODER_CLK_PORT PORTD
#define ENCODER_CLK_PORTLINE PD4

#define ENCODER_DT_DDR DDRD
#define ENCODER_DT_PIN PIND
#define ENCODER_DT_PORT PORTD
#define ENCODER_DT_PORTLINE PD3

#define ENCODER_SW_DDR DDRD
#define ENCODER_SW_PIN PIND
#define ENCODER_SW_PORT PORTD
#define ENCODER_SW_PORTLINE PD2

/*************
* Klasa encoder jest klas� pozwalaj�c� na obs�ug� enkodera interfejsu u�ytkownika. Zliczanie impuls�w generowanych przez styki enkodera realizowane jest za pomoc� przerwa� zewn�trznych.
* Klasa udost�pnia szereg metod, z kt�rych pomoc� mo�na odczytywa� i zapisywa� pozycj� pokr�t�a enkodera.
* oraz sprawdzi� stan wci�ni�cia wa�u enkodera. Klasa wykorzystuje Timer/Counter 0 mikrokontrolera w celu odfiltrowania drania styk�w.
*************/
class encoder{
	/*Pola prywatne klasy encoder */
	int16_t _Value; //zmienna 16 bitowa przechowuj�ca informacj� na temat po�o�enia pokr�t�a enkodera
	uint8_t _swState = 0; //zmienna przechowuj�ca stan wci�ni�cia wa�u enkodera

	
	public:
	/* metody publiczne klasy eeprom */
	
	/*Metoda encInit jest metod� inicjalizuj�c� linie mikrokontrolera pod��czone do enkodera */
	void encInit(void);
	
	/*Metoda getPosition jest metod� zwracaj�c� aktualn� pozycj� pokr�t�a enkodera */
	int16_t getPosition(void);
	
	/*Metoda setPosition jest metod� pozwalaj�ca na nadpisanie aktualnej pozycji pokr�t�a enkodera
	* parametry:
	* toSet - nowa pozycja pokr�t�a enkodera 
	*/
	void setPosition(int16_t toSet);
	
	/*Metoda getPosition jest metod� zwracaj�c� aktualny stan wci�ni�cia wa�u enkodera */
	uint8_t getSwState(void);
	
};



#endif /* ENCODER_H_ */
/**************
* plik: encoder.cpp
* PTM laboratoria, Projekt
* prowadzacy: Mgr in�. Andrzej Stachno
* zaj�cia: Y02-68p, pon. TP 9:15
* student: Mateusz W�jcik, 258973
* II rok AiR
* Politechnika Wroc�awska
*************/

#include "encoder.h"
#include "../GLOBAL.h"


/* Globalne zmienne do interakcji z rutynami przerwa� */
volatile uint32_t timercounter = 0;
volatile int16_t _Valueglb;
volatile uint8_t _glbSwState = 0;

void encoder::encInit(){
	    /*inicjacja wej��: ustawienie kierunk�w linii CLK, DT oraz SW jako wej�cia */
	    ENCODER_CLK_DDR&=!(1<<ENCODER_CLK_PORTLINE);
	    ENCODER_DT_DDR&=!(1<<ENCODER_DT_PORTLINE);
	    ENCODER_SW_DDR&=!(1<<ENCODER_SW_PORTLINE);
		
		/*W��czenie rezystor�w podci�gaj�cych linie CLK, DT oraz SW do linii zasilania mikrokontrolera */
	    ENCODER_CLK_PORT|=(1<<ENCODER_CLK_PORTLINE);
	    ENCODER_DT_PORT|=(1<<ENCODER_DT_PORTLINE);
	    ENCODER_SW_PORT|=(1<<ENCODER_SW_PORTLINE);
		
		
		/*konfiguracja wyzwalania przerwania INT0: na obu zboczach*/
		EICRA&=!(1<<ISC01);
		EICRA|=(1<<ISC00);
		
		/*Konfiguracja przerwa� zewn�trznych: przerwanie INT0 do �ledzenia pozycji pokr�t�a enkodera */
		EIMSK|=(1<<INT0); //w��czenie przerwania INT0
		


		//Konfiguracja przerwa� zewn�trznych: przerwanie INTI do �ledzenia stanu wci�ni�cia wa�u enkodera
	    EIMSK|=(1<<INT1); //w��czenie przerwania INT0
		
		/*konfiguracja wyzwalania przerwania INT0: na zboczu opadaj�cym */		
	    EICRA|=(1<<ISC10);
	    EICRA|=(1<<ISC11);


	    /*U�ycie Timera/Countera 2 pracuj�cego w trybie normal do filtracji drgania styk�w enkodera oraz rejestracji szybszych przekr�ce� pokr�t�a enkodera*/
		/*Konfiguracja:*/
	    /*Ustawienie prescalera (clk/64)*/
	    TCCR2B |= (1<<CS20);

		/*Ustawienie rejestru OCR2A: por�wnanie nast�puje co 500us*/
	    OCR2A = 63;

	    /*Ustawienie flagi OCF2A by zainicjowa� pierwsze przerwanie od por�wnania z OCR2A*/
	    TIFR2 |= (1<<OCF2A);
	    
		/*W��czenie przerwania od por�wnania z OCR2A*/
	    TIMSK2 |= (1<<OCIE2A);
		
		/*Wyzerowanie zmiennych globalnych u�ywanych w interakcji z rutynami przerwa�*/
		_Valueglb = 0;
		_Value = 0;
		
	}

void encoder::setPosition(int16_t toSet){
	_Valueglb = toSet;
	_Value = toSet;
}

int encoder::getPosition(void){
	_Value = _Valueglb;
	return _Value;
}

uint8_t encoder::getSwState(void){
	_swState = _glbSwState;
	return _swState; 
	
}


/* Rutyna przerwania od timera/countera 2 -> filtracja drga� styk�w oraz �ledzenie pr�dko�ci obrotowej wa�u enkodera*/
ISR(TIMER2_COMPA_vect)
{
	cli(); //wy��cz przerwania globalne
	TCNT2 = 0; //wyzeruj licznik 

	//zmienn� odpowiedzialn� za liczenie czasu mi�dzy kolejnymi impulsami jest zmienna timercounter
	//przerwanie wykona inkrementacj� tej zmiennej kiedy czas mi�dzy kolejnymi impulsami jest mniejszy ni� 1s
	if (timercounter < 2000)
	timercounter++;
	
	sei(); //w��cz globalne przerwania z powrotem
}

/* Rutyna przerwania zewn�trznego INT1 na linii DT -> �ledzenie impuls�w generowanych przez enkoder */
ISR(INT1_vect){
	/*przy wyzwoleniu przerwania sprawdzamy stan linii CLK w celu okre�lenia kierunku obrot�w.

	/*przy sprawdzaniu kierunku sprawdzana jest tak�e warto�� zmiennej timercounter by okre�li� pr�dko�� obrotu wa�u enkodera*/
	if (!(ENCODER_CLK_PIN & (1<<ENCODER_CLK_PORTLINE)) && (timercounter > 2)){ //CLK w stanie niskim -> obroty w lewo
		if(timercounter > 150)
			_Valueglb-= 1;
		else if(timercounter > 60)
			_Valueglb -= 5;
		else
			_Valueglb -= 10;
	}
	else if ( (ENCODER_CLK_PIN & (1<<ENCODER_CLK_PORTLINE)) && (timercounter > 2)){ //CLK w stanie wysokim -> obroty w prawo
		if(timercounter > 150)
			_Valueglb+= 1;
		else if(timercounter > 60)
			_Valueglb += 5;
		else 
			_Valueglb += 10;
	}

	timercounter = 0; //wyzeruj zmienn�, rozpocznij kolejne odliczanie
}


/* Rutyna przerwania zewn�trznego INT0 na linii SW -> �ledzenie stanu wci�ni�cia wa�u enkodera */
ISR(INT0_vect){

	
	
	if (!(ENCODER_SW_PIN & (1<<ENCODER_SW_PORTLINE)) && (timercounter > 10)) //po up�yni�ciu 5ms (filtracja drga� styk�w) sprawdzamy stan, je�li wyst�pi� stan niski, wa� jest wci�ni�ty
		_glbSwState = 1;

	else if ( (ENCODER_SW_PIN & (1<<ENCODER_SW_PORTLINE)) && (timercounter > 10)) //je�li stan linii jest wyoski wa� nie jest wci�ni�ty
		_glbSwState = 0;

	timercounter = 0;
}
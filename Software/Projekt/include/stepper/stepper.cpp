/**************
* plik: stepper.cpp
* PTM laboratoria, Projekt
* prowadzacy: Mgr in�. Andrzej Stachno
* zaj�cia: Y02-68p, pon. TP 9:15
* student: Mateusz W�jcik, 258973
* II rok AiR
* Politechnika Wroc�awska
*************/
#include "stepper.h"


volatile uint32_t pulses = 0;
volatile uint8_t rotationDir = 0;
volatile uint8_t stepperBusy = 0;
volatile uint16_t compareCounter = 0;
volatile uint16_t absolutePosition = 0;









void stepper::stepperInit(void){
	
	/*Inicjalizacja linii port� odpowiedzialnych za obs�ug� sterownika silnika krokowego: step, direction, enable*/
	/*Ustawienie kierunk�w linii enable, step, direction jako wyj�ciowe */
	STEPPER_EN_DDR|=(1<<STEPPER_EN_PORTLINE); 
	STEPPER_STEP_DDR|=(1<<STEPPER_STEP_PORTLINE);
	STEPPER_DIR_DDR|=(1<<STEPPER_DIR_PORTLINE);
	
	/*Ustawienie linii enable oraz step w stan wysoki w celu zapewnienia swobodnego poruszania si� silnika po uruchomieniu urz�dzenia*/
	STEPPER_EN_PORT|=(1<<STEPPER_EN_PORTLINE);
	STEPPER_STEP_PORT|=(1<<STEPPER_STEP_PORTLINE);
	
	
	/*inicjalizacja linii portu odpowiedzialnego za obs�ug� czujnika efektu halla. Linia ustawiona jako wej�cie.*/
	HALL_INPUT_DDR&=!(1<<HALL_INPUT_PORTLINE);
	
	/*Konfuguracja Timera/Countera 1*/
	
	/*Ustawienie trybu pracy timera jako CTC (clear timer on compare match) */
	TCCR1B|=(1<<WGM12);
	
	/*W��czenie przerwania od por�wnania rejestru licznika z rejestrem OCR1A*/
	TIMSK1|=(1<<OCIE1A);
	
	/*Pocz�tkowe ustawienie rejestru OCR1A jako 32000*/
	OCR1A = 32000;
	 
}


void stepper::stepperSetSpeed(uint16_t degPerSec){  
	/*Sterowanie pr�dko�ci� silnika odbywa si� za pomoc� zmiany warto�ci przechowywanej w rejestrze OCR1A - wi�ksza warto�� rejestru OCR1A powoduje wolniejsze obroty silnika*/
	/*w ciele metody wykonywane s� obliczenia warto�ci, kt�r� trzeba wpisa� do rejestru OCR1A w celu zapewnienia podawanej pr�dko�ci*/
	double a = (double) 80000/((double)TIMER1_PRESCALER * 2.0 * (double) degPerSec * (double) _stepperConf_stepsPerRevolution * (double) _stepperConf_uSteps);
	OCR1A = (uint16_t) (a * 36000.0); 
}


void stepper::stepperRun(){
	if(pulses!=0){
	/*Je�li licznik krok�w do wykonania jest pusty, w��cz zasilanie silnika i wystartuj timer*/
	STEPPER_EN_PORT&=!(1<<STEPPER_EN_PORTLINE);
	timerStart();
	stepperBusy = 1;	
	}
}


void stepper::planMove(uint16_t degreesToMove){
	/*w ciele tej metody dokonywane jest obliczenie ilo�ci impuls�w wygenerowanych przez timer/counter 1 w celu zapewnienia oczekiwanego obrotu silnika*/
	double ustepsPerDeg = ((double)_stepperConf_stepsPerRevolution) /360.0;
	pulses = (uint16_t) (2.0 * (double) degreesToMove * (double) _stepperConf_uSteps * ustepsPerDeg);
	
}


void stepper::timerStart(void){
	
	TCNT1 = 0; //wyzeruj licznik
	TCCR1B|=(1<<CS11); //wyzwolenie timera 1 nast�puje poprzez ustawienie �r�d�a zegara (prescaler = 1)
	compareCounter = 0;
}

void stepper::timerStop(void){
	//zatrzymanie timera odbywa si� przez od��czenie �r�d�a zegara
	TCCR1B&=0b11111000;	
}

void stepper::stepperStop(uint8_t disengage){
	pulses = 0;
	timerStop();
	stepperBusy = 0;
	stepperHomed = 0;	
	if(disengage) //je�li flaga disengage jest w stanie niskim, ustaw lini� en na stan wysoki (linia enalbe posiada aktywny stan niski)
		STEPPER_EN_PORT|=(1<<STEPPER_EN_PORTLINE);

}


uint8_t stepper::isStepperBusy(void){
		return stepperBusy;
}

uint16_t stepper::getStepsToTake(void){
	return pulses;
}

void stepper::setStepperDir(uint8_t rotation){
	if(rotation == 1)
		STEPPER_DIR_PORT|= (1<<STEPPER_DIR_PORTLINE);
	else
		STEPPER_DIR_PORT&=!(1<<STEPPER_DIR_PORTLINE);
	rotationDir = rotation;
}

uint16_t stepper::getAbsolutePosition(void){
	return absolutePosition;
}


uint8_t stepper::getStepperDir(){
	return rotationDir;
}

void stepper::clearPos(){
	absolutePosition = 0;
	pulses = 0;
}


void stepper::homeStepper(void){
	/*w ciele tej metody zerowane jest po�o�enie silnika za pomoc� czujnika efektu Halla*/
	/* W przypadku wykrycia braku sygna�u na wyj�ciu czujnika halla, silnik jest obracany dop�ki taki sygna� si� nie pojawi. Maksymalnie o dwa obroty*/
	/*Dodatkowo, przez to, �e obracanie z silnikiem wykonywane jest z ma�� pr�dko�ci�, pr�dko�� ta jest ustawiana, natomiast stara warto�� rejestru OCR1A jest zapisywana
	 oraz wczytana po zako�czeniu operacji zerowania*/
	uint16_t savedOCR1A = OCR1A;
if(stepperHomed == 0 ){  
	if(HALL_INPUT_PIN&(1<<HALL_INPUT_PORTLINE)){
		uint16_t savedOCR1A = OCR1A;
		stepperSetSpeed(50);
		planMove(720);
		stepperRun();
		setStepperDir(0);
		while(HALL_INPUT_PIN&(1<<HALL_INPUT_PORTLINE)){}
		stepperStop(0);
		planMove(720);
		stepperRun();
		setStepperDir(1);
		while(!(HALL_INPUT_PIN&(1<<HALL_INPUT_PORTLINE))){}
		stepperStop(0);
		absolutePosition = 0;
		pulses = 0;
		OCR1A = savedOCR1A;
	}else{
		stepperSetSpeed(50);
		planMove(720);
		stepperRun();
		setStepperDir(1);
		while(!(HALL_INPUT_PIN&(1<<HALL_INPUT_PORTLINE))){}
		stepperStop(0);
		absolutePosition = 0;
		pulses = 0;
		OCR1A = savedOCR1A;
		stepperHomed = 1;
		}
}else
	stepperHomed = 1;
}




/*Rutyna przerwania od por�wnania z rejestrem OCR1A: generowanie impuls�w dla wej�cia step w sterowniku silnika krokowego*/
ISR(TIMER1_COMPA_vect){
	/*w rutynie g��wn� wykonywan� czynno�ci� jest prze��czanie stanu linii step naprzemiennie, generuj�c sygna� prostok�tny o wype�nieniu 50%
	oraz regulowanej cz�stotliwo�ci, zale�nej od warto�ci w rejestrze OCR1A*/
cli();
++compareCounter;
if((pulses - compareCounter)>0){ //linia step jest prze��czana tak d�ugo, jak d�ugo licznik impuls�w nie zr�wna si� z liczb� wymagan� dla zaplanowanego ruchu
	STEPPER_STEP_PORT^=(1<<STEPPER_STEP_PORTLINE);
}else{
	if(rotationDir) //dalej licznik pozycji bezwzgl�dnej jest aktualizowany z ka�dym wykonanym impulsem
		absolutePosition = absolutePosition + pulses;
	else
		absolutePosition = absolutePosition - pulses;
	pulses = 0;
	compareCounter = 0;
	TCCR1B&=0b11111000; //po wykonaniu ruchu timer 1 jest wy��czany, a wszystkie liczniki impuls�w oraz flaga zaj�to�ci silnika zostaj� wyzerowane
	stepperBusy = 0;
}
sei();
	
}


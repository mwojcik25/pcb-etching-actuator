/**************
* plik: stepper.h
* PTM laboratoria, Projekt
* prowadzacy: Mgr in�. Andrzej Stachno
* zaj�cia: Y02-68p, pon. TP 9:15
* student: Mateusz W�jcik, 258973
* II rok AiR
* Politechnika Wroc�awska
*************/


#ifndef STEPPER_H_
#define STEPPER_H_
#include "../GLOBAL.h"


/* Konfiguracja linii mikrokontrolera pod��czonych do sterownika silnika krokowego (TMC2208)*/
#define STEPPER_EN_DDR DDRD
#define STEPPER_EN_PORT PORTD
#define STEPPER_EN_PIN PIND
#define STEPPER_EN_PORTLINE PD1

#define STEPPER_STEP_DDR DDRD
#define STEPPER_STEP_PORT PORTD
#define STEPPER_STEP_PIN PIND
#define STEPPER_STEP_PORTLINE PD5

#define STEPPER_DIR_DDR DDRD
#define STEPPER_DIR_PORT PORTD
#define STEPPER_DIR_PIN PIND
#define STEPPER_DIR_PORTLINE PD6

#define HALL_INPUT_DDR DDRB
#define HALL_INPUT_PORT PORTB
#define HALL_INPUT_PIN PINB
#define HALL_INPUT_PORTLINE PB2

#define TIMER1_PRESCALER 8



/*************
* Klasa stepper jest klas� pozwalaj�c� na obs�ug� silnika krokowego, steruj�c pod��czonym do linii kontrolera sterownikiem silnik�w krokowych z interfejsem step/direction.
* Klasa posiada zaimplementowane metody umo�liwiaj�ce wygodne obracanie wa�em silnika z dan� pr�dko�ci� oraz o zadany k�t. Wspiera tak�e zerowanie pozycji wa�u silnika za pomoc� 
* Czujnika po�o�enia (czujnik efektu Halla).
*
* Klasa wykorzystuje 16-bitowy timer/counter1 do generacji przebiegu fali prostok�tnej dla wej�cia step w sterowniku.
***********/
class stepper{



	/*metody publiczne klasy*/
	public:

	/*Metoda stepperInit jest metod� inicjuj�c� linie port�w pod��czone do sterownika.
	 *Odpowiada tak�e za pocz�tkow� konfiguracj� timera/countera 1*/
	void stepperInit(void);
	
	/*Metoda stepperRun odpowiada za wyzwolenie ruchu silnika krokowego. Przed wywo�aniem metody konieczne jest zaplanowanie ruchu metod� planMove. */	
	void stepperRun(void);
	
	/*Metoda stepperStop natychmiastowo zatrzymuje silnik, gdy znajduje si� on w ruchu. Przekazanie "1" do parametru disengage wy��cza sterownik.
	* Wpisanie "0" do tej flagi powoduje jedynie zatrzymanie ruchu silnika, ale nie powoduje odci�cia zasilania. W�wczas pozycja silnika podtrzymywana jest przez moment trzymaj�cy. */
	void stepperStop(uint8_t disengage);
	
	/*Metoda stepperSetSpeed odpowiada za ustawienie pr�dko�ci, z jak� ma porusza� si� silnik przy nast�pnym zaplanowanym ruchu. W parametrze przekazywana jest pr�dko�� w stopniach na sekund�*/
	void stepperSetSpeed(uint16_t degPerSec);
	
	/*Metoda planMove odpowiada za zaplanowanie nast�pnego ruchu silnika krokowego. W parametrze przekazywany jest k�t obrotu w stopniach.
	*Zaplanowany ruch b�dzie odbywa� si� z pr�dko�ci� oraz kierunkiem ustawionymi w momencie wywo�ania metody.*/
	void planMove(uint16_t degreesToMove);
	
	/*Metoda isStepperBusy zwraca stan silnika krokowego. Metoda zwraca "1" je�li silnik krokowy jest w trakcie wykonywania ruchu, zwraca "0" je�li silnik nie obraca si�.*/
	uint8_t isStepperBusy(void);
	
	/* Metod� setStepperDir mo�emy ustawi� kierunek nast�pnego ruchu silnika, wpisuj�c "1" w parametrze dla obrot�w zgodnie w prawo, "0" dla obrot�w w lewo. */ 
	void setStepperDir(uint8_t rotation);
	
	/*Metoda getStepperDir zwraca ustawiony kierunek obrot�w silnika krokowego ze stanami zgodnymi z poprzedni� metod�. */
	uint8_t getStepperDir(void);
	
	/*Metoda getStepperToTake zwraca ilo�� krok�w do wykonania w ramach aktualnie zaplanowanego ruchu */
	uint16_t getStepsToTake(void);
	
	/*Metoda getAbsolutePosition zwraca bezwzgl�dn� pozycj� wa�u silnika krokowego. Pozycja jest �ledzona zgodnie z zaplanowanymi ruchami oraz ich kierunkami. */
	uint16_t getAbsolutePosition(void);
	
	/*Metoda clearPos pozwala na wyzerowanie wszystkich zaplanowanych ruch�w oraz pozycji bezwzgl�dnej silnika krokowego. */
	void clearPos(void);
	
	/*Metoda homeStepper pozwala na wyzerowanie pozycji wa�u silnika krokowego w oparciu o czujnik pozcyji zrealizowany za pomoc� czujnika efektu Halla. */
	void homeStepper(void);
	
	
	
	
	


	
	private:
	/*zmienne prywatne przechowuj�ce parametry u�ytego silnika*/
	uint8_t _stepperConf_uSteps =2; //ustawienie podzia�u na mikrokroki 1 krok = 2 mikrokroki
	uint16_t _stepperConf_stepsPerRevolution = 200; //ustawienie u�ytego silnika: 200 krok�w/obr�t
	
	/*inne zmienne prywatne*/
	uint32_t _actualPosition = 0; //zmienna przechowuj�ca aktualn� (bezwzgl�dn�) pozycj� wa�u silnika 
	uint8_t stepperHomed = 0; //flaga przechowuj�ca stan wyzerowania wa�u silnika za pomoc� czujnika po�o�enia
	
	
	
	/*metody prywatne klasy stepper*/
	/*metoda prywatna timerStart odpowiada za wyzwolenie timera/countera 1 u�ytego do generacji sygna�u dla wej�cia step*/
	void timerStart(void);
	
	/*Wywo�anie metody timerStop spowoduje zatrzymanie dzia�ania timera/countera 1*/
	void timerStop(void);
	
};

#endif /* STEPPER_H_ */
/*
 * LCD_HD44780.cpp
 *
 *  Created on: 12-10-2013
 *      Author: Wojtek
 */

//-------------------------------------------------------------------------------------------------
// Wy�wietlacz alfanumeryczny ze sterownikiem HD44780
// Sterowanie w trybie 4-bitowym bez odczytu flagi zaj�to�ci
// z dowolnym przypisaniem sygna��w steruj�cych
// Plik : HD44780.c
// Mikrokontroler : Atmel AVR
// Kompilator : avr-gcc
// Autor : Rados�aw Kwiecie�
// �r�d�o : http://radzio.dxp.pl/hd44780/
// Data : 24.03.2007
//-------------------------------------------------------------------------------------------------
#include "LCD_HD44780.h"

//-------------------------------------------------------------------------------------------------
//
// Procedura inicjalizacji kontrolera HD44780.
//
//-------------------------------------------------------------------------------------------------





void LCD_HD44780::init(void) {
	unsigned char i;
	LCD_DB4_DIR |= LCD_DB4; // Konfiguracja kierunku pracy wyprowadze�
	LCD_DB5_DIR |= LCD_DB5; //
	LCD_DB6_DIR |= LCD_DB6; //
	LCD_DB7_DIR |= LCD_DB7; //
	LCD_E_DIR |= LCD_E;   //
	LCD_RS_DIR |= LCD_RS;  //
	_delay_ms(15); // oczekiwanie na ustalibizowanie si� napiecia zasilajacego
	LCD_RS_PORT &= ~LCD_RS; // wyzerowanie linii RS
	LCD_E_PORT &= ~LCD_E;  // wyzerowanie linii E

	for (i = 0; i < 3; i++) // trzykrotne powt�rzenie bloku instrukcji
			{
		LCD_E_PORT |= LCD_E; //  E = 1
		LCD_HD44780::outNibble(0x03); // tryb 8-bitowy
		LCD_E_PORT &= ~LCD_E; // E = 0
		_delay_ms(5); // czekaj 5ms
	}

	LCD_E_PORT |= LCD_E; // E = 1
	LCD_HD44780::outNibble(0x02); // tryb 4-bitowy
	LCD_E_PORT &= ~LCD_E; // E = 0

	_delay_ms(1); // czekaj 1ms
	LCD_HD44780::writeCommand(
			HD44780_FUNCTION_SET | HD44780_FONT5x7 | HD44780_TWO_LINE
					| HD44780_4_BIT); // interfejs 4-bity, 2-linie, znak 5x7
	LCD_HD44780::writeCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_OFF); // wy��czenie wyswietlacza
	LCD_HD44780::writeCommand(HD44780_CLEAR); // czyszczenie zawartos�i pamieci DDRAM
	_delay_ms(2);
	LCD_HD44780::writeCommand(
			HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT); // inkrementaja adresu i przesuwanie kursora
	LCD_HD44780::writeCommand(
			HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_OFF
					| HD44780_CURSOR_NOBLINK); // w��cz LCD, bez kursora i mrugania
					
		//init 8 custom chars
		uint8_t ch=0, chn=0;
		while(ch<64)
		{
			LCDdefinechar((LcdCustomChar+ch),chn++);
			ch=ch+8;
		}
	}


//-------------------------------------------------------------------------------------------------
//
// Funkcja wystawiaj�ca p�bajt na magistral� danych
//
//-------------------------------------------------------------------------------------------------
void LCD_HD44780::outNibble(unsigned char nibbleToWrite) {
	if (nibbleToWrite & 0x01)
		LCD_DB4_PORT |= LCD_DB4;
	else
		LCD_DB4_PORT &= ~LCD_DB4;

	if (nibbleToWrite & 0x02)
		LCD_DB5_PORT |= LCD_DB5;
	else
		LCD_DB5_PORT &= ~LCD_DB5;

	if (nibbleToWrite & 0x04)
		LCD_DB6_PORT |= LCD_DB6;
	else
		LCD_DB6_PORT &= ~LCD_DB6;

	if (nibbleToWrite & 0x08)
		LCD_DB7_PORT |= LCD_DB7;
	else
		LCD_DB7_PORT &= ~LCD_DB7;
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu bajtu do wy�wietacza (bez rozr�nienia instrukcja/dane).
//
//-------------------------------------------------------------------------------------------------
void LCD_HD44780::write(unsigned char dataToWrite) {
	LCD_E_PORT |= LCD_E;
	LCD_HD44780::outNibble(dataToWrite >> 4);
	LCD_E_PORT &= ~LCD_E;
	LCD_E_PORT |= LCD_E;
	LCD_HD44780::outNibble(dataToWrite);
	LCD_E_PORT &= ~LCD_E;
	_delay_us(50);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu rozkazu do wy�wietlacza
//
//-------------------------------------------------------------------------------------------------
void LCD_HD44780::writeCommand(unsigned char commandToWrite) {
	LCD_RS_PORT &= ~LCD_RS;
	LCD_HD44780::write(commandToWrite);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja zapisu danych do pami�ci wy�wietlacza
//
//-------------------------------------------------------------------------------------------------
void LCD_HD44780::writeData(unsigned char dataToWrite) {
	LCD_RS_PORT |= LCD_RS;
	LCD_HD44780::write(dataToWrite);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja wy�wietlenia napisu na wyswietlaczu.
//
//-------------------------------------------------------------------------------------------------
void LCD_HD44780::writeText(char * text) {
	while (*text)
		LCD_HD44780::writeData(*text++);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja ustawienia wsp�rz�dnych ekranowych
//
//-------------------------------------------------------------------------------------------------
void LCD_HD44780::goTo(unsigned char x, unsigned char y) {
	LCD_HD44780::writeCommand(HD44780_DDRAM_SET | (x + (0x40 * y)));
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja czyszczenia ekranu wy�wietlacza.
//
//-------------------------------------------------------------------------------------------------
void LCD_HD44780::clear(void) {
	LCD_HD44780::writeCommand(HD44780_CLEAR);
	_delay_ms(2);
}
//-------------------------------------------------------------------------------------------------
//
// Funkcja przywr�cenia pocz�tkowych wsp�rz�dnych wy�wietlacza.
//
//-------------------------------------------------------------------------------------------------
void LCD_HD44780::home(void) {
	LCD_HD44780::writeCommand(HD44780_HOME);
	_delay_ms(2);
}
//
//Wyswietla na wyswietlaczu LCD liczbe przekazana w argumencie funkcji
void LCD_HD44780::printNumber(int32_t liczba, uint8_t x, uint8_t y) {
	//LCD_HD44780::clear();
	char text[16];
	sprintf(text, "%d", liczba);
	if (liczba > 0){
		
		if(liczba < 1000){
			LCD_HD44780::goTo(x+3, y);
			LCD_HD44780::writeData(' ');
		}
		
		if(liczba < 100){
			LCD_HD44780::goTo(x+2, y);
			LCD_HD44780::writeData(' ');
			LCD_HD44780::goTo(x+1, y);
			LCD_HD44780::writeData(' ');
		}
		if(liczba < 10){
			LCD_HD44780::goTo(x+3, y);
			LCD_HD44780::writeData(' ');
			LCD_HD44780::goTo(x+2, y);
			LCD_HD44780::writeData(' ');
			LCD_HD44780::goTo(x+1, y);
			LCD_HD44780::writeData(' ');
		}
	}
		
	else if (liczba < 0){
		if(liczba > -100){
			LCD_HD44780::goTo(x+3, y);
			LCD_HD44780::writeData(' ');
		}
		if(liczba > -10){
			LCD_HD44780::goTo(x+4, y);
			LCD_HD44780::writeData(' ');
			LCD_HD44780::goTo(x+2, y);
			LCD_HD44780::writeData(' ');
		}
	}
	else if (liczba == 0){
		LCD_HD44780::goTo(x+1, y);
		LCD_HD44780::writeData(' ');
		LCD_HD44780::goTo(x+2, y);
		LCD_HD44780::writeData(' ');
		LCD_HD44780::goTo(x+3, y);
		LCD_HD44780::writeData(' ');
		LCD_HD44780::goTo(x+4, y);
		LCD_HD44780::writeData(' ');
	}
		
	LCD_HD44780::goTo(x,y);
	LCD_HD44780::writeText(text);
}


void  LCD_HD44780::progressBar(uint16_t progress, uint16_t maxprogress, uint8_t length){
	uint16_t i;
	uint16_t pixelprogress;
	uint16_t c;

	// draw a progress bar displaying (progress / maxprogress)
	// starting from the current cursor position
	// with a total length of "length" characters
	// ***note, LCD chars 0-5 must be programmed as the bar characters
	// char 0 = empty ... char 5 = full

	// total pixel length of bargraph equals length*PROGRESSPIXELS_PER_CHAR;
	// pixel length of bar itself is
	pixelprogress = ((progress*(length*PROGRESSPIXELS_PER_CHAR))/maxprogress);
	
	// print exactly "length" characters
	for(i=0; i<length; i++)
	{
		// check if this is a full block, or partial or empty
		// (u16) cast is needed to avoid sign comparison warning
		if( ((i*(uint16_t)PROGRESSPIXELS_PER_CHAR)+5) > pixelprogress )
		{
			// this is a partial or empty block
			if( ((i*(uint16_t)PROGRESSPIXELS_PER_CHAR)) > pixelprogress )
			{
				// this is an empty block
				// use space character?
				c = 0;
			}
			else
			{
				// this is a partial block
				c = pixelprogress % PROGRESSPIXELS_PER_CHAR;
			}
		}
		else
		{
			// this is a full block
			c = 5;
		}
		
		// write character to display
		LCD_HD44780::writeData(c);
	}

}

void LCD_HD44780::LCDdefinechar(const uint8_t *pc,uint8_t char_code){
	uint8_t a, pcc;
	uint16_t i;
	a=(char_code<<3)|0x40;
	for (i=0; i<8; i++){
		pcc=pgm_read_byte(&pc[i]);
		LCD_HD44780::writeCommand(a++);
		LCD_HD44780::writeData(pcc);
	}
}


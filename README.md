# PCB Etching Actuator

A device for actuating PCB etchant for faster etching.

## Project pictures

![Front view](./Docs/pictures/front.png "Front view")
![Back view](./Docs/pictures/back.png "Back view")
![Left view](./Docs/pictures/left.png "Left view")
![Right view](./Docs/pictures/right.png "Right view")


## Electronics
[Schematic](./Docs/pdfs/schematic.pdf)

### Board views

![Top layer](./Docs/pictures/board_top.png "Top layer")
![Bottom layer](./Docs/pictures/board_bottom.png "Bottom layer")

### Board 3D model
![Top](./Docs/pictures/board_3d_top.png "Top")
![Bottom](./Docs/pictures/board_3d_bottom.png "Bottom")

## Firmware
[Source code](./Software/Projekt/)
